<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'logo_path' => 'image/cL3AMI1GmjdnyIBbEMJtp0hPV253ltyS4bcnHMQq.jpg',
            'address' => 'sdfs',
            'doc_path' => 'pdf/o4mjynxLzGjiDhHUaRjljSS6R8mlWazZTByqmo9A.pdf',
            'website' => 'zzzz.com'
        ];
    }
}
