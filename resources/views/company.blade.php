@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Company') }}

                    <a href="{{ url('/company/create') }}" class="btn btn-success btn-sm float-right">Create New Company</a>
                </div>


                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div style="overflow-x:auto;">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Website</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Document</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(isset($data))
                                @foreach($data as $key=>$dt)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{$dt->name}}</td>
                                    <td>{{$dt->address}}</td>
                                    <td>{{$dt->email}}</td>
                                    <td>{{$dt->website}}</td>
                                    <td>
                                        <a href="{{asset('storage/'.$dt->logo_path)}}" download="">
                                            <img src="{{asset('storage/'.$dt->logo_path)}}" alt="" width="100" height="100">
                                        </a>
                                    </td>
                                    <td>
                                        @if($dt->doc_path)
                                        <a href="{{asset('storage/'.$dt->doc_path)}}" download="">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                                                <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                                                <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                                            </svg>
                                        </a>
                                        @endif

                                    </td>
                                    <td class="row">
                                        <div class="mr-2">
                                            <a href="{{url('company/'.$dt->id)}}" class="btn btn-info">Edit</a>
                                        </div>

                                        <form action="{{url('company/'.$dt->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>


                    </div>

                    <div class="">
                        {!! $data->links('pagination::bootstrap-4') !!}
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection