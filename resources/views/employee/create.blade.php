@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Employee Details') }}

                </div>


                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


                    <form action="{{url('/employee')}}{{isset($id) ?'/'.$id:''}}" method="post" enctype="multipart/form-data">
                        @csrf
                        {{isset($id) ? method_field('PATCH'):''}}
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">First Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ $first_name ?? old('first_name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Last Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ $last_name ?? old('last_name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Company</label>
                            <div class="col-sm-8">
                                <select name="company" id="" class="form-control">
                                    <option value="">Select Company</option>
                                    @if(isset($company))
                                    @foreach($company as $ct)
                                    <option value="{{$ct->id}}" {{ (isset($cid) && $cid==$ct->id) ? 'selected' : '' }}>{{$ct->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="email" placeholder="Email" value="{{  $email ?? old('email')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Phone</label>
                            <div class="col-sm-8">
                                <div class="custom-file">
                                    <input type="number" name="phone" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                        </div>

                        <div class="form-group d-flex justify-content-center">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    {{ isset($id) ? 'Update' : 'Save' }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection