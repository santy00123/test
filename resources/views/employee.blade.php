@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Employee') }}

                    <a href="{{url('/employee/create')}}" class="btn btn-success btn-sm float-right">Create New Employee</a>
                </div>


                <div class="card-body">

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div style="overflow-x:auto;">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Company</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(isset($data))
                                @foreach($data as $key=>$dt)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td>{{$dt->first_name}}</td>
                                    <td>{{$dt->last_name}}</td>
                                    <td>{{$dt->company}}</td>
                                    <td>{{$dt->email}}</td>
                                    <td>{{$dt->phone}}</td>
                                    <td class="row">
                                        <div class="mr-2">
                                            <a href="{{url('employee/'.$dt->id)}}" class="btn btn-info">Edit</a>
                                        </div>

                                        <form action="{{url('employee/'.$dt->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>


                    </div>

                    <div class="">
                        {!! $data->links('pagination::bootstrap-4') !!}
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection