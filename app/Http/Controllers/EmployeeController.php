<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employee::leftJoin('companies', function ($join) {
            $join->on('employees.company', '=', 'companies.id');
        })
            ->whereNull('employees.deleted_at')
            ->select([
                'employees.id',
                'employees.first_name',
                'employees.last_name',
                'companies.name as company',
                'employees.email',
                'employees.phone'
            ])->paginate(10);

        return view('employee')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
        return view('employee.create')->with('company', $company);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->saveUpdate($request))
            return redirect('/employee');
        else
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employee::leftJoin('companies', function ($join) {
            $join->on('employees.company', '=', 'companies.id');
        })
            ->where('employees.id', $id)
            ->whereNull('employees.deleted_at')
            ->first([
                'employees.id',
                'employees.first_name',
                'employees.last_name',
                'employees.company as cid',
                'companies.name as company',
                'employees.email',
                'employees.phone'
            ]);


        $data->company = Company::all();
        return view('employee.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($this->saveUpdate($request, $id) == true)
            return redirect('/employee');
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();

        return redirect('employee');
    }

    public function saveUpdate($request, $id = null)
    {

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required'
        ]);


        if (isset($id) && $id > 0)
            Employee::where('id', $id)->delete();

        Employee::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        return true;
    }
}
