<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Company::whereNull('deleted_at')->paginate(10);

        return view('company')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->saveUpdate($request))
            return redirect('/company');
        else
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Company::where('id', $id)->whereNull('deleted_at')->first();

        return view('company.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->saveUpdate($request, $id) == true)
            return redirect('/company');
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::where('id', $id)->delete();

        return redirect('company');
    }

    public function saveUpdate($request, $id = null)
    {

        $validated = $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'pdf' => 'mimes:pdf|max:10000'
        ]);


        // $path = Storage::putFile('image', new File('/path/to/image'), 'public');



        if ($request->file('image')) {
            $imgPath = $request->file('image')->store(
                'image',
                'public'
            );
        }


        if ($request->file('pdf')) {
            $pdfPath = $request->file('pdf')->store(
                'pdf',
                'public'
            );
        }

        if (isset($id) && $id > 0)
            Company::where('id', $id)->delete();

        Company::create([
            'name' => $request->name,
            'address' => $request->address,
            'website' => $request->website,
            'email' => $request->email,
            'logo_path' => $imgPath ?? "",
            'doc_path' => $pdfPath ?? ""
        ]);

        return true;
    }
}
